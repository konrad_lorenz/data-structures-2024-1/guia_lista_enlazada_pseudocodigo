
# Manual de Implementación de Lista Enlazada en Python - Konrad Lorenz

## Paso 1: Definir el Nodo

1. **Crear una clase llamada `Nodo`**. Esta clase representará los elementos individuales de la lista enlazada.
2. **Inicializar el nodo**:
   - Añade un método `__init__` a la clase `Nodo` que tome un argumento `valor`. Este valor es el dato que el nodo almacenará.
   - Dentro de este método, asigna el `valor` pasado al atributo `valor` del nodo.
   - Establece el atributo `siguiente` del nodo a `None`. Esto indica que, inicialmente, el nodo no está enlazado a otro nodo siguiente.
3. **Representación de cadena del nodo**:
   - Añade un método `__str__` a la clase `Nodo` que simplemente retorne el valor del nodo como una cadena de caracteres. Esto facilitará visualizar el contenido del nodo cuando sea necesario.

## Paso 2: Crear la Lista Enlazada

1. **Definir una clase llamada `ListaEnlazada`**. Esta clase manejará la colección de nodos enlazados entre sí.
2. **Inicializar la lista enlazada**:
   - Añade un método `__init__` a la clase `ListaEnlazada`. Este método no tomará ningún argumento aparte de `self`.
   - Dentro de este método, establece el atributo `cabeza` a `None`. Esto indica que la lista está inicialmente vacía.

## Paso 3: Insertar Nodos

1. **Añadir un método `insertar` a la clase `ListaEnlazada`**:
   - Este método tomará un argumento `valor`, el cual es el dato que se desea añadir a la lista.
   - Crea un nuevo nodo con este valor.
   - Si la lista está vacía (la `cabeza` es `None`), establece el nuevo nodo como la cabeza de la lista.
   - Si la lista ya contiene nodos, encuentra el último nodo de la lista y enlaza el nuevo nodo a este último nodo.

## Paso 4: Buscar en la Lista

1. **Implementar un método `buscar` en `ListaEnlazada`**:
   - Este método toma un `valor` como argumento y busca este valor en la lista.
   - Recorre los nodos de la lista desde la cabeza.
   - Si encuentra un nodo cuyo valor coincide con el valor buscado, retorna `True`.
   - Si llega al final de la lista sin encontrar el valor, retorna `False`.

## Paso 5: Mostrar los Nodos

1. **Añadir un método `mostrar` a la clase `ListaEnlazada`**:
   - Este método no toma argumentos y su propósito es imprimir los valores de los nodos de la lista enlazada.
   - Recorre la lista desde la cabeza, imprimiendo el valor de cada nodo seguido de `->` para indicar la conexión entre los nodos.
   - Al final de la lista, simplemente imprime una nueva línea para terminar la salida de manera ordenada.

## Uso de la Lista Enlazada

Para usar esta implementación de la lista enlazada:
1. **Instancia la clase `ListaEnlazada`** para crear una nueva lista enlazada.
2. **Utiliza el método `insertar`** para añadir elementos a la lista.
3. **Usa el método `buscar`** para verificar si un valor específico existe en la lista.
4. **Emplea el método `mostrar`** para visualizar los elementos de la lista.

Siguiendo estos pasos, puedes implementar y manipular una lista enlazada básica en Python, añadiendo nodos, buscando valores específicos dentro de la lista y visualizando los contenidos de la lista.
